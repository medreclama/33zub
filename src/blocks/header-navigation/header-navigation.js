const headerNavigation = () => {
  const parents = document.querySelectorAll('.header-navigation__item--parent:not(.header-navigation__item--sub), .header-navigation__item--parent.header-navigation__item--sub-1');
  Array.from(parents).forEach((parent) => {
    const link = parent.querySelector('a');
    const opener = document.createElement('div');
    opener.classList.add('header-navigation__opener');
    opener.addEventListener('click', (e) => {
      e.target.classList.toggle('header-navigation__opener--active');
    });
    link.after(opener);
  });
};

export default headerNavigation;
