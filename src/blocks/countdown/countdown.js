const countdowns = document.querySelectorAll('.countdown');

const addCountdown = (deadline) => {
  let addTimer = null;
  const daysVal = document.querySelector('.countdown__days .countdown__val');
  const hoursVal = document.querySelector('.countdown__hours .countdown__val');
  const minutesVal = document.querySelector('.countdown__minutes .countdown__val');
  const secondsVal = document.querySelector('.countdown__seconds .countdown__val');

  const timeCount = () => {
    const now = new Date();
    const diff = deadline - now;
    if (diff <= 0) {
      clearInterval(addTimer);
    }
    const days = diff > 0 ? (`0${Math.floor(diff / 1000 / 60 / 60 / 24)}`).slice(-2) : '00';
    const hours = diff > 0 ? (`0${Math.floor(diff / 1000 / 60 / 60) % 24}`).slice(-2) : '00';
    const minutes = diff > 0 ? (`0${Math.floor(diff / 1000 / 60) % 60}`).slice(-2) : '00';
    const seconds = diff > 0 ? (`0${Math.floor(diff / 1000) % 60}`).slice(-2) : '00';
    daysVal.textContent = days;
    hoursVal.textContent = hours;
    minutesVal.textContent = minutes;
    secondsVal.textContent = seconds;
  };
  timeCount();
  addTimer = setInterval(timeCount, 1000);
};

const countdown = () => {
  countdowns.forEach((cd) => {
    if (cd.dataset.date) {
      const date = cd.dataset.date.split(',');
      addCountdown(new Date(...date));
    }
  });
};

export default countdown;
