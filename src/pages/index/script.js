import headerSwitcher from '../../blocks/header-switcher/header-switcher';
import headerNavigation from '../../blocks/header-navigation/header-navigation';

document.addEventListener('DOMContentLoaded', () => {
  headerSwitcher();
  headerNavigation();
});
