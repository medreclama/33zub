import countdown from '../../blocks/countdown/countdown';
import tabs from '../../blocks/tabs/tabs';
import Slider from '../../blocks/slider/slider';

document.addEventListener('DOMContentLoaded', () => {
  countdown();
  tabs();
  const landingStaff = document.querySelector('.landing-staff .slider__slides');
  const staffSlider = new Slider(landingStaff, {
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      800: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      1000: {
        slidesPerView: 5,
        spaceBetween: 30,
      },
    },
  });
  const landingFeedback = document.querySelector('.landing-feedback .slider__slides');
  staffSlider.init();
  const feedbackSlider = new Slider(landingFeedback, {
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 30,
      },
      800: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
    },
  });
  feedbackSlider.init();
});
