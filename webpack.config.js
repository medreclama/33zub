const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';

module.exports = {
  mode,
  entry: {
    script: './src/pages/index/script.js',
    main: './src/pages/main/main.js',
    landing: './src/pages/landing/landing.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].js',
  },
  devtool: isProduction ? undefined : 'source-map',
  plugins: [new ESLintPlugin()],
  optimization: {
    minimize: isProduction,
  },
};
