import gulp from 'gulp';
import changed from 'gulp-changed';

import { distDir } from '../gulpfile.babel';

gulp.task('copy', (done) => {
  gulp.src(['src/resources/**'])
    .pipe(changed(distDir))
    .pipe(gulp.dest(distDir));
  done();
});
