import gulp from 'gulp';
import gulpIf from 'gulp-if';
import stylus from 'gulp-stylus';
import plumber from 'gulp-plumber';
import sourcemaps from 'gulp-sourcemaps';
import importIfExist from 'stylus-import-if-exist';
import autoprefixer from 'autoprefixer-stylus';
import gcmq from 'gulp-group-css-media-queries';
import cleanCSS from 'gulp-clean-css';
import rename from 'gulp-rename';

import { isDebug, templateDir } from '../gulpfile.babel';

gulp.task('stylus', (done) => {
  gulp.src(['./src/styles/*.styl', './src/pages/**/*.styl'])
    .pipe(plumber())
    .pipe(gulpIf(isDebug, sourcemaps.init()))
    .pipe(stylus({
      use: [
        importIfExist(),
        autoprefixer(),
      ],
      'include css': true,
    }))
    .pipe(gulpIf(!isDebug, gcmq()))
    .pipe(gulpIf(!isDebug, cleanCSS({
      level: {
        2: {
          all: true,
        },
      },
    })))
    .pipe(gulpIf(isDebug, sourcemaps.write()))
    .pipe(rename({
      dirname: '',
    }))
    .pipe(gulp.dest(`${templateDir}styles`));
  done();
});
